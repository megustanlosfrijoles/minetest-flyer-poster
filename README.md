# Minetest flyer / poster

## About

Share the love with Minetest!  
In each zip file you'll find an .svg, .pdf and .png version. Use .svg to make edits and .pdf to print it!  

**BEWARE**: zip files containing "_OLD" in their name use **non-libre** fonts, so *do not* use them: they belong to a past version of the flyer, in which fonts were wrongly licensed. The only reason I've kept these files is to make life easier for whoever wants to fix them. 

If you want to contribute, you can:
* translate it and open a MR (please provide all the files that you find in the other ZIPs)
* print it and set it up all around you
  
The current artwork is under CC BY-SA 4.0 and it's made by Zughy

## Credits 

**Fonts**  
Avenipixel by Gabriel Sammartino  
Dogica Pixel by Roberto Mocci  
Pixeloid Sans by GGBotNet  
Silkscreen by Jason Kottke (parenthesis used in IT)  
Titan One by Rodrigo Fuenzalida  
  
**Icons (all modified by me, and also quoted in the back of the image as they're CC BY-SA 4.0)**  
Sparkles and gift by Laura Humpfer  
Artist palette by Martin Wehl  
Family by Fanny Jung  

**Translations**  
English by Zughy, freshreplicant, MisterE  
German by Marc Mader  
Indonesian by srifqi  
Italian by Zughy  
